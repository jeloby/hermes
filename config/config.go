package config

import (
	"os"
	"time"

	"github.com/BurntSushi/toml"
)

// Config stores daemon configuration.
type Config struct {
	General generalConfig `toml:"general"`
	// Server Config
	Server Server `toml:"server"`
	GRPC   Server `toml:"grpc_api"`
	// Message Broker Config
	Nats     nats     `toml:"nats"`
	RabbitMQ rabbitmq `toml:"rabbitmq"`
	// Database Config
	PSQL  psqlConfig `toml:"psql"`
	Redis redis      `toml:"redis"`
}

// NewConfig creates a new Config
func NewConfig(file, name string) (*Config, error) {
	c := Config{}

	if _, err := os.Stat(file); err != nil {
		file = "/etc/" + name + "/config.toml"
	}

	if _, err := toml.DecodeFile(file, &c); err != nil {
		return &c, err
	}

	return &c, nil
}

// Local duration type that satisfies the encoding.TextUnmarshaler interface.
type duration struct {
	time.Duration
}

// Unmarshal a duration form the configuration to a time.Duration.
func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))

	return err
}

// General configuration section.
type generalConfig struct {
	AuthToken string `toml:"authtoken"`
}

// PSQL configuration section.
type psqlConfig struct {
	DSN      string `toml:"dsn"`
	Host     string `toml:"host"`
	Port     string `toml:"port"`
	User     string `toml:"user"`
	Password string `toml:"password"`
	DBName   string `toml:"dbname"`
}

// Redis configuation section.
type redis struct {
	Address  string `toml:"address"`
	Password string `toml:"password"`
	DB       int    `toml:"db"`
}

// Mesh stores mesh configuration.
type nats struct {
	Servers        []string      `toml:"servers"`
	User           string        `toml:"username"`
	Password       string        `toml:"password"`
	TLS            *tlsConfig    `toml:"tls"`
	RequestTimeout duration      `toml:"requesttimeout"`
	Options        optionsConfig `toml:"options"`
}

// rabbitmq stores rabbitmq configuration.
type rabbitmq struct {
	Enabled        bool          `toml:"enabled"`
	User           string        `toml:"username"`
	Password       string        `toml:"password"`
	Host           string        `toml:"host"`
	Port           string        `toml:"port"`
	TLS            *tlsConfig    `toml:"tls"`
	RequestTimeout duration      `toml:"requesttimeout"`
	Options        optionsConfig `toml:"options"`
	Exchange       Exchange      `toml:"exchange"`
}

type Exchange struct {
	Name string `toml:"name"`
	Type string `toml:"type"`
}

type tlsConfig struct {
	CACert string `toml:"cacert"`
	Cert   string `toml:"cert"`
	Key    string `toml:"key"`
}

type optionsConfig struct {
	RandomServerPool bool `toml:"randomserverpool"`
}

// API configuration section.
type Server struct {
	Enabled            bool       `toml:"enabled"`
	Server             string     `toml:"server"`
	Port               string     `toml:"port"`
	Secret             string     `toml:"secret"`
	HTTPRequestTimeout duration   `toml:"httprequesttimeout"`
	TokenTimeout       duration   `toml:"tokentimeout"`
	TempDir            string     `toml:"tempdir"`
	TLS                *tlsConfig `toml:"tls"`
}
