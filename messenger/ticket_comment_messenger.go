package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/queues"
	"bitbucket.org/jeloby/go_utils/models/tickets"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/hermes/models"
)

type TicketCommentService struct {
	// Cache   *redis.
	DB      *nats_worker.Connection
	Log     *logger.Log
	Timeout time.Duration
}

func (tc *TicketCommentService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.TicketComment, *common.Pagination, error) {
	var (
		err  error
		resp tickets.TicketCommentResponse
	)

	var req = tickets.TicketCommentRequest{Method: common.ModelCRUD_SelectAll, Filter: filter, Pagination: pagination}

	err = tc.DB.RequestReplyPb(queues.TicketCommentModel.Request(), &req, &resp, tc.Timeout)
	if err != nil {
		return nil, nil, err
	}

	if resp.GetError() != "" {
		return nil, nil, errors.New(resp.GetError())
	}

	var users = make([]*models.TicketComment, len(resp.GetTicketComments()))
	for i, comment := range resp.GetTicketComments() {
		users[i] = &models.TicketComment{TicketComment: comment}
	}

	return users, resp.Pagination, nil
}

func (tc *TicketCommentService) Select(filter *common.Filter) (*models.TicketComment, error) {
	var (
		err  error
		resp tickets.TicketCommentResponse
	)

	var req = tickets.TicketCommentRequest{Method: common.ModelCRUD_Select, Filter: filter}

	err = tc.DB.RequestReplyPb(queues.TicketCommentModel.Request(), &req, &resp, tc.Timeout)
	if err != nil {
		return nil, err
	}

	if resp.GetError() != "" {
		return nil, errors.New(resp.GetError())
	}

	return &models.TicketComment{TicketComment: resp.GetTicketComments()[0]}, nil
}

func (tc *TicketCommentService) Create(model *models.TicketComment) error {
	var (
		err  error
		resp tickets.TicketCommentResponse
	)

	var req = tickets.TicketCommentRequest{Method: common.ModelCRUD_Create, TicketComment: model.TicketComment}

	err = tc.DB.RequestReplyPb(queues.TicketCommentModel.Request(), &req, &resp, tc.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}

func (tc *TicketCommentService) Update(model *models.TicketComment) error {
	var (
		err  error
		resp tickets.TicketCommentResponse
	)

	var req = tickets.TicketCommentRequest{Method: common.ModelCRUD_Update, TicketComment: model.TicketComment}

	err = tc.DB.RequestReplyPb(queues.TicketCommentModel.Request(), &req, &resp, tc.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}

func (tc *TicketCommentService) Delete(filter *common.Filter) error {
	var (
		err  error
		resp tickets.TicketCommentResponse
	)

	var req = tickets.TicketCommentRequest{Method: common.ModelCRUD_SelectAll, Filter: filter}

	err = tc.DB.RequestReplyPb(queues.TicketCommentModel.Request(), &req, &resp, tc.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}
