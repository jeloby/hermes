package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/queues"
	"bitbucket.org/jeloby/go_utils/models/tickets"
	"bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/hermes/models"
)

type TicketService struct {
	// Cache   *redis.
	DB      *nats_worker.Connection
	Log     *logger.Log
	Timeout time.Duration
}

func (t *TicketService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.Ticket, *common.Pagination, error) {
	var (
		err  error
		resp tickets.TicketResponse
	)

	var req = tickets.TicketRequest{Method: common.ModelCRUD_SelectAll, Filter: filter, Pagination: pagination}

	err = t.DB.RequestReplyPb(queues.TicketModel.Request(), &req, &resp, t.Timeout)
	if err != nil {
		return nil, nil, err
	}

	if resp.GetError() != "" {
		return nil, nil, errors.New(resp.GetError())
	}

	var users = make([]*models.Ticket, len(resp.GetTickets()))
	for i, ticket := range resp.GetTickets() {
		users[i] = &models.Ticket{Ticket: ticket}
	}

	return users, resp.Pagination, nil
}

func (t *TicketService) Select(filter *common.Filter) (*models.Ticket, error) {
	var (
		err  error
		resp tickets.TicketResponse
	)

	var req = tickets.TicketRequest{Method: common.ModelCRUD_Select, Filter: filter}

	err = t.DB.RequestReplyPb(queues.TicketModel.Request(), &req, &resp, t.Timeout)
	if err != nil {
		return nil, err
	}

	if resp.GetError() != "" {
		return nil, errors.New(resp.GetError())
	}

	return &models.Ticket{Ticket: resp.GetTickets()[0]}, nil
}

func (t *TicketService) Create(model *models.Ticket) error {
	var (
		err  error
		resp tickets.TicketResponse
	)

	var req = tickets.TicketRequest{Method: common.ModelCRUD_Create, Ticket: model.Ticket}

	err = t.DB.RequestReplyPb(queues.TicketModel.Request(), &req, &resp, t.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}

func (t *TicketService) Update(model *models.Ticket) error {
	var (
		err  error
		resp tickets.TicketResponse
	)

	var req = tickets.TicketRequest{Method: common.ModelCRUD_Update, Ticket: model.Ticket}

	err = t.DB.RequestReplyPb(
		queues.TicketModel.Request(),
		&req,
		&resp,
		t.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}

func (t *TicketService) Delete(filter *common.Filter) error {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{Method: common.ModelCRUD_Delete, Filter: filter}

	err = t.DB.RequestReplyPb(
		queues.TicketModel.Request(),
		&req,
		&resp,
		t.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}
