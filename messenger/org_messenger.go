package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/org"
	"bitbucket.org/jeloby/go_utils/models/queues"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/hermes/models"
)

type OrgService struct {
	// Cache   *redis.
	DB      *nats_worker.Connection
	Log     *logger.Log
	Timeout time.Duration
}

func (u *OrgService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.Org, *common.Pagination, error) {
	var (
		err  error
		resp org.OrgResponse
	)

	var req = org.OrgRequest{
		Method:     common.ModelCRUD_SelectAll,
		Filter:     filter,
		Pagination: pagination,
	}

	err = u.DB.RequestReplyPb(
		queues.OrgModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return nil, nil, err
	}

	var companies = make([]*models.Org, len(resp.GetOrgs()))
	for i, c := range resp.GetOrgs() {
		companies[i] = &models.Org{Org: c}
	}

	return companies, resp.Pagination, nil
}

func (u *OrgService) Select(filter *common.Filter) (*models.Org, error) {
	var (
		err  error
		resp org.OrgResponse
	)

	var req = org.OrgRequest{
		Method: common.ModelCRUD_Select,
		Filter: filter,
	}

	err = u.DB.RequestReplyPb(
		queues.OrgModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return nil, err
	}

	if resp.Error != "" {
		return nil, errors.New(resp.Error)
	}

	return &models.Org{Org: resp.GetOrgs()[0]}, nil
}

func (u *OrgService) Create(model *models.Org) error {
	var (
		err  error
		resp org.OrgResponse
	)

	var req = org.OrgRequest{
		Method: common.ModelCRUD_Create,
		Org:    model.Org,
	}

	err = u.DB.RequestReplyPb(
		queues.OrgModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}

func (u *OrgService) Update(model *models.Org) error {
	var (
		err  error
		resp org.OrgResponse
	)

	var req = org.OrgRequest{
		Method: common.ModelCRUD_Update,
		Org:    model.Org,
	}

	err = u.DB.RequestReplyPb(
		queues.OrgModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}

func (u *OrgService) Delete(filter *common.Filter) error {
	var (
		err  error
		resp org.OrgResponse
	)

	var req = org.OrgRequest{
		Method: common.ModelCRUD_Delete,
		Filter: filter,
	}

	err = u.DB.RequestReplyPb(
		queues.OrgModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}
