package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/nats_worker"
)

type Manager struct {
	nc      *nats_worker.Connection
	timeout time.Duration
}

func New(conn *nats_worker.Connection, timeout int64) *Manager {
	return &Manager{
		nc:      conn,
		timeout: time.Duration(timeout),
	}
}
