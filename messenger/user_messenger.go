package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/queues"
	"bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/hermes/models"
)

type UserService struct {
	// Cache   *redis.
	DB      *nats_worker.Connection
	Log     *logger.Log
	Timeout time.Duration
}

func (u *UserService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.User, *common.Pagination, error) {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{
		Method:     common.ModelCRUD_SelectAll,
		Filter:     filter,
		Pagination: pagination,
	}

	err = u.DB.RequestReplyPb(
		queues.UserModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return nil, nil, err
	}

	var users = make([]*models.User, len(resp.GetUsers()))
	for i, u := range resp.GetUsers() {
		users[i] = &models.User{User: u}
	}

	return users, resp.Pagination, nil
}

func (u *UserService) Select(filter *common.Filter) (*models.User, error) {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{
		Method: common.ModelCRUD_Select,
		Filter: filter,
	}

	err = u.DB.RequestReplyPb(
		queues.UserModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return nil, err
	}

	if resp.Error != "" {
		return nil, errors.New(resp.Error)
	}

	return &models.User{User: resp.GetUsers()[0]}, nil
}

func (u *UserService) Create(model *models.User) error {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{
		Method: common.ModelCRUD_Create,
		User:   model.User,
	}

	err = u.DB.RequestReplyPb(
		queues.UserModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}

func (u *UserService) Update(model *models.User) error {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{
		Method: common.ModelCRUD_Update,
		User:   model.User,
	}

	err = u.DB.RequestReplyPb(
		queues.UserModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}

func (u *UserService) Delete(filter *common.Filter) error {
	var (
		err  error
		resp user.UserResponse
	)

	var req = user.UserRequest{
		Method: common.ModelCRUD_Delete,
		Filter: filter,
	}

	err = u.DB.RequestReplyPb(
		queues.UserModel.Request(),
		&req,
		&resp,
		u.Timeout,
	)
	if err != nil {
		return err
	}

	if resp.Error != "" {
		return errors.New(resp.Error)
	}

	return nil
}
