package messenger

import (
	"time"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/queues"
	"bitbucket.org/jeloby/go_utils/models/tickets"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/hermes/models"
)

type TicketWatcherService struct {
	// Cache   *redis.
	DB      *nats_worker.Connection
	Log     *logger.Log
	Timeout time.Duration
}

func (tw *TicketWatcherService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.TicketWatcher, *common.Pagination, error) {
	var (
		err  error
		resp tickets.TicketWatcherResponse
	)

	var req = tickets.TicketWatcherRequest{Method: common.ModelCRUD_SelectAll, Filter: filter, Pagination: pagination}

	err = tw.DB.RequestReplyPb(queues.TicketWatcherModel.Request(), &req, &resp, tw.Timeout)
	if err != nil {
		return nil, nil, err
	}

	if resp.GetError() != "" {
		return nil, nil, errors.New(resp.GetError())
	}

	var users = make([]*models.TicketWatcher, len(resp.GetTicketWatchers()))
	for i, watcher := range resp.GetTicketWatchers() {
		users[i] = &models.TicketWatcher{TicketWatcher: watcher}
	}

	return users, resp.Pagination, nil
}

func (tw *TicketWatcherService) Create(model *models.TicketWatcher) error {
	var (
		err  error
		resp tickets.TicketWatcherResponse
	)

	var req = tickets.TicketWatcherRequest{Method: common.ModelCRUD_Create, TicketWatcher: model.TicketWatcher}

	err = tw.DB.RequestReplyPb(queues.TicketWatcherModel.Request(), &req, &resp, tw.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}

func (tw *TicketWatcherService) Update(model *models.TicketWatcher) error {
	var (
		err  error
		resp tickets.TicketWatcherResponse
	)

	var req = tickets.TicketWatcherRequest{Method: common.ModelCRUD_Update, TicketWatcher: model.TicketWatcher}

	err = tw.DB.RequestReplyPb(queues.TicketWatcherModel.Request(), &req, &resp, tw.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}

func (tw *TicketWatcherService) Delete(filter *common.Filter) error {
	var (
		err  error
		resp tickets.TicketWatcherResponse
	)

	var req = tickets.TicketWatcherRequest{Method: common.ModelCRUD_SelectAll, Filter: filter}

	err = tw.DB.RequestReplyPb(queues.TicketWatcherModel.Request(), &req, &resp, tw.Timeout)
	if err != nil {
		return err
	}

	if resp.GetError() != "" {
		return errors.New(resp.GetError())
	}

	return nil
}
