# Ticket Router

## Select All

##### Action: GET
##### URL: https://localhost:8000/api/tickets?q=company_id='<ticket_id>'

##### Request
```
curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X GET "https://localhost:8000/api/tickets?q=company_id='<ticket_id>'"
```

##### Response
```
{
  "status":"ok",
  "data":[
    {
      "id":"testssss",
      "company_id":"testssss",
      "user_id":"testssss",
      "customer_company":"testssss",
      "customer_company_id":"testssss",
      "description":"testssss",
      "timestamp":12345678979
    },
    ...
  ],
  "pagination":{"size":10,"total":3}
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Select

##### Action: GET
##### URL: https://localhost:8000/api/tickets/<ticket_id>

##### Request
```
curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X GET "https://localhost:8000/api/tickets/<ticket_id>"
```

##### Response
```
{
  "status":"ok",
  "data": {
    "id":"testssss",
    "company_id":"testssss",
    "user_id":"testssss",
    "customer_company":"testssss",
    "customer_company_id":"testssss",
    "description":"testssss",
    "timestamp":12345678979
  }
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Create
##### Action: POST
##### URL: https://localhost:8000/api/tickets

##### Payload
```
{ 
  "type": 0, 
  "company_id": "testssss", 
  "user_id": "testssss", 
  "customer_email": "testssss", 
  "customer_company": "testssss", 
  "customer_company_id": "testssss", 
  "description": "testssss", 
  "status": 0, 
}
```

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X POST "https://localhost:8000/api/tickets" -d '{ "id": "testssss", "type": 0, "company_id": "testssss", "user_id": "testssss", "customer_email": "testssss", "customer_company": "testssss", "customer_company_id": "testssss", "description": "testssss", "status": 0, "timestamp": 12345678979}'

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Update

##### Action: PUT
##### URL: https://localhost:8000/api/tickets/<ticketID>

##### Payload
```
{ 
  "type": 0, 
  "company_id": "testssss", 
  "user_id": "testssss", 
  "customer_email": "testssss", 
  "customer_company": "testssss", 
  "customer_company_id": "testssss", 
  "description": "testssss", 
  "status": 0, 
}
```

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X PUT "https://localhost:8000/api/tickets<ticketID>" -d '{"type": 0, "company_id": "testssss", "user_id": "testssss", "customer_email": "testssss", "customer_company": "testssss", "customer_company_id": "testssss", "description": "testssss", "status": 0}'

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Delete

##### Action: DELETE
##### URL: https://localhost:8000/api/tickets/<ticketID>

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X DELETE "https://localhost:8000/api/tickets/<ticketID>"

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```