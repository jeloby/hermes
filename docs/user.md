# Ticket Router

## Select All

##### Action: GET
##### URL: https://localhost:8000/api/users?q=org_id='<userID>'

##### Request
```
curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X GET "https://localhost:8000/api/users?q=org_id='<userID>'"
```

##### Response
```
{
  "status":"ok",
  "data":[
    {
      "id":"1ec6f91d-977a-6ec0-ac50-16146939c26b",
      "org_id":"03a1453a-949e-4613-9032-862d0567dfd2",
      "first_name":"Stefan",
      "last_name":"Moller",
      "email":"stefanmoller@live.com",
      "phone":"0828955572",
      "created_by":"03a1453a-949e-4613-9032-862d0567dfd4",
      "created_at":1641543275,
      "updated_at":1641543275
    },
    {
      "id":"1ec6f91f-04f7-618c-ac50-f7833924c02b",
      "org_id":"03a1453a-949e-4613-9032-862d0567dfd2",
      "first_name":"Stefan",
      "last_name":"Moller",
      "email":"stefanmoller1@live.com",
      "phone":"0828955572",
      "created_by":"03a1453a-949e-4613-9032-862d0567dfd4",
      "created_at":1641543313,
      "updated_at":1641543313
    }
  ],
  "pagination":{"size":10,"total":2}}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Select

##### Action: GET
##### URL: https://localhost:8000/api/users/<userID>

##### Request
```
curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X GET "https://localhost:8000/api/users/<userID>"
```

##### Response
```
{
  "status":"ok",
  "data":{
    "id":"1ec6f91d-977a-6ec0-ac50-16146939c26b",
    "org_id":"03a1453a-949e-4613-9032-862d0567dfd2",
    "first_name":"Stefan",
    "last_name":"Moller",
    "email":"stefanmoller@live.com",
    "phone":"0828955572",
    "created_by":"03a1453a-949e-4613-9032-862d0567dfd4",
    "created_at":1641543275,
    "updated_at":1641543275
  }
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Create
##### Action: POST
##### URL: https://localhost:8000/api/users

##### Payload
```
{
	"org_id": "03a1453a-949e-4613-9032-862d0567dfd2",
	"first_name": "Stefan",
	"last_name": "Moller",
	"email": "stefanmoller@live.com",
	"phone": "0828955572",
	"password": "helloworld",
	"role": 0,
	"account_type": 0,
	"account_status": 0,
	"payload": "",
	"created_by": "03a1453a-949e-4613-9032-862d0567dfd4"
}
```

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X POST "https://localhost:8000/api/users" -d '{ "org_id": "03a1453a-949e-4613-9032-862d0567dfd2", "first_name": "Stefan", "last_name": "Moller", "email": "stefanmoller@live.com", "phone": "0828955572", "password": "helloworld", "role": 0, "account_type": 0, "account_status": 0, "payload": "", "created_by": "03a1453a-949e-4613-9032-862d0567dfd4" }'

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Update

##### Action: PUT
##### URL: https://localhost:8000/api/users/<userID>

##### Payload
```
{ 
  "type": 0, 
  "org_id": "testssss", 
  "user_id": "testssss", 
  "customer_email": "testssss", 
  "customer_company": "testssss", 
  "customer_company_id": "testssss", 
  "description": "testssss", 
  "status": 0, 
}
```

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X PUT "https://localhost:8000/api/users<userID>" -d '{"type": 0, "org_id": "testssss", "user_id": "testssss", "customer_email": "testssss", "customer_company": "testssss", "customer_company_id": "testssss", "description": "testssss", "status": 0}'

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```

## Delete

##### Action: DELETE
##### URL: https://localhost:8000/api/users/<userID>

##### Request

curl  -H 'Content-Type: application/json' -H -i 'Accept: */*' -X DELETE "https://localhost:8000/api/users/<userID>"

##### Response
```
{
  "status":"ok",
}
```

##### Error
```
{
  "status":"error",
  "error":"some error message"
}
```