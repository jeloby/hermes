# Auth Routes

## Login
Route: `/api/auth/login`
```
Payload: 
{
  "email": stefanmoller@live.com, 
  "password": "strongPassword123"
}
Ok Response: 
{
  "status": "ok", 
  "user": {#usermodel}
}
Error Response:
{
  "status":"error",
  "error":"incorrect user credentials"
}
```

## Register

Route: `/api/auth/register`
```
Payload: 
{
  "user": {#usermodel}, 
  "company": {#companymodel}
}
Ok Response: 
{
  "status": "ok", 
  "user": {#usermodel}
}
Error Response:
{
  "status":"error",
  "error":"incorrect user credentials"
}
```