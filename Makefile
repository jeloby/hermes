## A simple makefile to manage this service.

name=hermes
version=$(shell grep -m 1 version main.go | awk '{print $$3}' | tr -d '"')
packageversion=1
packagename=$(name)-$(version)-$(packageversion)
build=main.buildStamp=`date -u '+%Y-%m-%d_%I:%M:%S%p'`
githash=main.gitHash=`git rev-parse HEAD`
gittag=v$(version)
maintainer="Stefan Moller"
description="LOGSERV (Logistics derver)"
builddir=/tmp/$(name)/build
ldflags=-ldflags="-s -w -X $(build) -X $(githash)"
cmd=@go run -race $(ldflags) `ls cmd/*.go | grep -v '_test.go'` -c etc/$(name)/config.toml
gopath=${GOPATH}
ifeq ($(gopath),)
gopath=${HOME}/go
endif

GOFILES_NOVENDOR = $(shell find . -type f -name '*.go' -not -path "./vendor/*")

dev: 
	$(cmd) --debug

run: lint dev

profile-block: run --profile=block

profile-cpu: run --profile=cpu

profile-mem: run --profile=mem

test:
	@go test ./... --failfast -race $(ldflags) -v -run=^$

cover:
	@go test ./... -race -benchtime, -cpu -cover

lint: lint-wsl
	@golangci-lint run -v

lint-wsl: 
	@find . -type f -name "*.go" -exec ~/go/bin/wsl {} \;

benchmark:
	go test -run=. -bench=. -benchtime=5s -count 5 -benchmem -cpuprofile=cpu.out -memprofile=mem.out -trace=trace.out ./router | tee bench.txt

clean:
	rm -f $(gopath)/bin/$(name)
	mkdir -p $(builddir)
	sudo rm -fr $(builddir)/*

install: lint clean
	@go install $(ldflags) cmd/hermes.go

build: install

webapp:
	echo "Building webapps"
	rm -rf public/*
	for file in `ls $$DELIVERIESFE`; do \
		echo $$file ; \
		yarn --cwd $$DELIVERIESFE/$$file build; \
		cp -r $$DELIVERIESFE/$$file/dist/* ./public/; \
	done

loginapp:
	echo "Building webapps"
	yarn --cwd $$DELIVERIESFE/loginapp build; \
	cp -r $$DELIVERIESFE/loginapp/dist/* ./public/; \

adminapp:
	echo "Building webapps"
	yarn --cwd $$DELIVERIESFE/adminapp build; \
	cp -r $$DELIVERIESFE/adminapp/dist/* ./public/; \

mainapp:
	echo "Building webapps"
	yarn --cwd $$DELIVERIESFE/mainapp build; \
	cp -r $$DELIVERIESFE/mainapp/dist/* ./public/; \


build-escape:
	@go build -gcflags="-m -m" $(ldflags)

migrate:
	migrate -path ./db/migrations/ -database postgres://jclog:logisticsServerDB67890-@localhost:5432/jclog_db up 2

package: install
	echo version is $(version)
	echo 2.0 > $(builddir)/debian-binary
	echo "Package:" $(name) > $(builddir)/control
	echo "Version:" $(version) >> $(builddir)/control
	echo "Architecture: amd64" >> $(builddir)/control
	echo "Section: net" >> $(builddir)/control
	echo "Maintainer:" $(maintainer) >> $(builddir)/control
	echo "Priority: optional" >> $(builddir)/control
	echo "Description:" $(description) >> $(builddir)/control
	echo " Built" `date`
	sudo rm -rf $(builddir)/usr
	mkdir -p $(builddir)/usr/local/bin
	mkdir -p $(builddir)/etc
	cp $(gopath)/bin/$(name) $(builddir)/usr/local/bin/
	cp -r etc/* $(builddir)/etc/
	rm -f $(builddir)/etc/${name}/config.toml
	rm -f $(builddir)/etc/${name}/tls/*
	find $(builddir)/etc/${name}/ -maxdepth 1 -type f | grep -v '.example' | rm -f
	sudo chown -R root:root $(builddir)/usr
	sudo chown -R root:root $(builddir)/etc
	sudo chmod 640 $(builddir)/etc/$(name)/config.toml.example
	sudo tar cvzf $(builddir)/data.tar.gz -C $(builddir) usr etc
	sudo tar cvzf $(builddir)/control.tar.gz -C $(builddir) control
	git tag $(gittag)
	git push origin tag $(gittag)
	cd $(builddir) && sudo ar rc $(packagename).deb debian-binary control.tar.gz data.tar.gz && cd ..
