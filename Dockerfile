FROM fedora:33

RUN mkdir /app
WORKDIR /app

COPY go.mod /app
COPY go.sum /app
COPY -r ./etc/hermes/ /etc/hermes/
COPY -r ./etc/tls/ /etc/tls/

COPY -r cmd /app/
COPY -r config /app/
COPY -r messenger /app/
COPY -r models /app/
COPY -r public /app/
COPY -r router /app/




# COPY ../go_utils ./

# RUN echo "replace bitbucket.org/jeloby/go_utils => ../go_utils"

COPY . /app


# RUN useradd -m jeloby
RUN mkdir -p /home/.ssh
# RUN chown -R jeloby:jeloby /home/jeloby/.ssh

VOLUME /Users/stefanmoller/.ssh /home/.ssh

# USER jeloby

# RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"
# RUN export GOPRIVATE=bitbucket.org/jeloby/go_utils

# RUN go build -o hermes cmd/hermes.go

EXPOSE 8000

CMD [ "/bin/sh" ]