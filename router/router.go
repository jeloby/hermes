package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

// Router returns a net/http compliant http.Router.
func (m *Manager) Router() http.Handler {
	r := chi.NewRouter()
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)
	// r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Compress(-1))
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(m.HTTPRequestTimeout))

	// API Router
	r.Mount("/api", m.apiRouter())
	r.Mount("/internal", m.internalRouter())
	r.Mount("/", m.fileServer())

	return r
}

func (m *Manager) apiRouter() http.Handler {
	r := chi.NewRouter()
	r.Route("/auth", m.authRouter)
	r.Route("/users", m.userRouter)
	r.Route("/orgs", m.orgsRouter)

	r.Route("/tickets", m.ticketRouter)
	r.Route("/tickets/{ticketID}/comment", m.ticketCommentRouter)
	r.Route("/tickets/{ticketID}/watcher", m.ticketWatcherRouter)

	return r
}
