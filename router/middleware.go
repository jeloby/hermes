package router

import (
	"context"
	"net/http"
	"strconv"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/models/common"
	"github.com/go-chi/render"
)

type ContextKey string

const (
	// Context Handlers
	filterContext     ContextKey = "filter"
	paginationContext ContextKey = "pagination"
)

// Filter middleware is used to load a splats.Filter object into context.
func (m *Manager) Filter(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var safe string

		filter := &common.Filter{Raw: r.FormValue("q")}
		_, _, _, _, err := filter.Parse()
		if err != nil {
			safe, err = errors.Wrapper(err, "cannot parse filter")
			render.JSON(w, r, &BasicResponse{Status: "error", Error: safe})

			m.log.Errorf("%v", err)

			return
		}

		ctx := context.WithValue(r.Context(), filterContext, filter)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Filter middleware is used to load a splats.Filter object into context.
func (m *Manager) SetSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error

		session, _ := store.Get(r, SessionName)
		session.Values["foo"] = "bar"
		session.Values[42] = 43
		err = session.Save(r, w)
		if err != nil {
			render.JSON(w, r, &BasicResponse{Status: "error", Error: "unable to create session"})
		}

		// ctx := context.WithValue(r.Context(), filterContext, filter)
		// next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Filter middleware is used to load a splats.Filter object into context.
func (m *Manager) ValidateSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			err  error
			safe string
			// ctx  context.Context
			// cache *utils.CachedSession
		)

		defer func() {
			if err != nil {
				render.JSON(w, r, &BasicResponse{Status: "error", Error: safe})
				return
			}
			// ctx = context.WithValue(r.Context(), filterContext, cache)
			// next.ServeHTTP(w, r.WithContext(ctx))
		}()

		session, err := store.Get(r, SessionName)
		if err != nil {
			safe = "invalid or expired session"
		}
		sessionKey := session.Values[SessionKey]
		m.log.Debugf("Session: %#v", sessionKey)

		if sessionKey == nil {
			safe, err = errors.Wrapper(err, sessionLoadFailed)
			return
		}

		// cache, err = m.SessionStore.Get(sessionKey.(string))
		// m.Log.Debugf("cache: %#v", cache)
	})
}

// Paginate middleware is used to load a splats.Pagination object into context
func (m *Manager) Paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		page, err := strconv.Atoi(r.URL.Query().Get("page"))
		if err != nil {
			page = 0
		}

		size, err := strconv.Atoi(r.URL.Query().Get("page_size"))
		if err != nil || size == 0 {
			size = 10
		}

		ctx := context.WithValue(r.Context(), paginationContext, &common.Pagination{Current: int32(page), Size: int32(size)})
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
