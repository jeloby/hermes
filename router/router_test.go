package router

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/hermes/config"
	"github.com/go-chi/chi/v5"
)

type testAPICaller struct {
	manager  *Manager
	router   *chi.Mux
	handler  http.Handler
	recorder *httptest.ResponseRecorder
}

func createRouterTest() testAPICaller {
	// Create new manager
	log := logger.New(true, "test_suite")
	manager := NewManager(config.Server{}, log, nil)
	manager.UserService = UserService{}

	// Create router and manager
	r := chi.NewRouter()
	handler := r.Middlewares().Handler(manager.Router())
	recorder := httptest.NewRecorder()

	return testAPICaller{
		manager:  manager,
		router:   r,
		handler:  handler,
		recorder: recorder,
	}
}

func (m *testAPICaller) useFilter() {
	m.router.Use(m.manager.Filter)
}

func (m *testAPICaller) usePagination() {
	m.router.Use(m.manager.Paginate)
}

func (m *testAPICaller) serve(req *http.Request) {
	m.handler.ServeHTTP(m.recorder, req)
}

func (m *testAPICaller) request(method, route string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, route, body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")

	return req, nil
}

func requestBody(in interface{}) *bytes.Buffer {
	data := new(bytes.Buffer)

	err := json.NewEncoder(data).Encode(in)
	if err != nil {
		log.Fatalln(err)
	}

	return data
}

func checkFilter(filter *common.Filter) error {
	if filter == nil {
		return errors.New("missing filter")
	}

	if filter.Raw == "" {
		return errors.New("empty filter")
	}

	return nil
}

func checkPagination(pagination *common.Pagination) error {
	if pagination == nil {
		return errors.New("missing pagination")
	}

	if pagination.GetCurrent() == 0 && pagination.GetSize() == 0 {
		return errors.New("empty pagination")
	}

	return nil
}
