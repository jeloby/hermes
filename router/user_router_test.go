package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"testing"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/hermes/models"
)

var mockGetUsers = []*user.User{ // User test response
	{
		ID:                "03a1453a-949e-4613-9032-862d0567dfd1",
		OrgID:             "03a1453a-949e-4613-9032-862d0567dfd2",
		FirstName:         "Stefan",
		LastName:          "Moller",
		Email:             "stefanmoller@live.com",
		Phone:             "0828955572",
		Role:              common.Role_Admin,
		PasswordUpdatedAt: 1641205072,
		Payload:           "",
		NotesID:           "03a1453a-949e-4613-9032-862d0567dfd3",
		CreatedBy:         "03a1453a-949e-4613-9032-862d0567dfd4",
		CreatedAt:         1641205072,
		UpdatedAt:         1641215072,
		DeletedAt:         0,
	},
}

var mockCreateUser = []*user.User{ // User create request
	{
		OrgID:     "03a1453a-949e-4613-9032-862d0567dfd2",
		FirstName: "Stefan",
		LastName:  "Moller",
		Email:     "stefanmoller@live.com",
		Phone:     "0828955572",
		Password:  "helloworld",
		Role:      common.Role_Admin,
		Payload:   "",
		CreatedBy: "03a1453a-949e-4613-9032-862d0567dfd4",
	},
}

var mockUpdateUser = []*user.User{ // User create request
	{
		ID:        "03a1453a-949e-4613-9032-862d0567dfd1",
		OrgID:     "03a1453a-949e-4613-9032-862d0567dfd2",
		FirstName: "Stefan",
		LastName:  "Moller",
		Email:     "stefanmoller@live.com",
		Phone:     "0828955572",
		Password:  "helloworld",
		Role:      common.Role_Admin,
		Payload:   "",
		NotesID:   "03a1453a-949e-4613-9032-862d0567dfd3",
		CreatedBy: "03a1453a-949e-4613-9032-862d0567dfd4",
	},
}

type UserService struct{}

func (u UserService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.User, *common.Pagination, error) {
	if err := checkFilter(filter); err != nil {
		return nil, nil, err
	}

	if err := checkPagination(pagination); err != nil {
		return nil, nil, err
	}

	mocks := mockGetUsers

	users := make([]*models.User, len(mocks))
	for i, user := range mocks {
		users[i] = &models.User{User: user}
	}

	return users, nil, nil
}

func (u UserService) Select(filter *common.Filter) (*models.User, error) {
	if err := checkFilter(filter); err != nil {
		return nil, err
	}

	user := models.User{}

	id := strings.SplitAfter(filter.Raw, "=")[1]
	id = strings.Replace(id, "'", "", -1)
	fmt.Println(id)

	mocks := mockGetUsers
	for _, u := range mocks {
		if u.GetID() == strings.TrimSpace(id) {
			user = models.User{User: u}
		}
	}

	return &user, nil
}

func (u UserService) Create(model *models.User) error {
	return model.Validate()
}

func (u UserService) Update(model *models.User) error {
	return model.Validate()
}

func (u UserService) Delete(filter *common.Filter) error {
	if err := checkFilter(filter); err != nil {
		return err
	}

	return nil
}

func TestSelectAllUsers(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := http.NewRequest("GET", "/api/users?q=company_id=12345", nil)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Error("Error in request")
	}
}

func TestSelectUser(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	for _, u := range mockGetUsers {
		url := "/api/users/" + u.GetID()

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			t.Errorf("Error creating a new request: %v", err)
		}

		caller.serve(req)

		if status := caller.recorder.Code; status != http.StatusOK {
			t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
		}

		var resp BasicResponse
		if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
			t.Errorf("Error decoding response body: %v", err)
		}

		if resp.Status != "ok" {
			t.Error("Error in request")
		}

		data := resp.Data.(map[string]interface{})
		if data["id"] != u.GetID() {
			t.Error("Incorrect user selected")
		}
	}
}

func TestCreateUser(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	mockData := mockCreateUser
	for _, mockUser := range mockData {
		body := requestBody(&mockUser)

		req, err := caller.request("POST", "/api/users", body)
		if err != nil {
			t.Errorf("Error creating a new request: %v", err)
		}

		caller.serve(req)

		if status := caller.recorder.Code; status != http.StatusOK {
			t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
		}

		var resp BasicResponse
		if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
			t.Errorf("Error decoding response body: %v", err)
		}

		if resp.Status != "ok" {
			t.Errorf("Error in request: %#v", resp.Error)
		}
	}
}

func TestUpdateUser(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	mockData := mockUpdateUser
	for _, mockUser := range mockData {
		req, err := caller.request("PUT", "/api/users/03a1453a-949e-4613-9032-862d0567dfd1", requestBody(mockUser))
		if err != nil {
			t.Errorf("Error creating a new request: %v", err)
		}

		caller.serve(req)

		if status := caller.recorder.Code; status != http.StatusOK {
			t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
		}

		var resp BasicResponse
		if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
			t.Errorf("Error decoding response body: %v", err)
		}

		if resp.Status != "ok" {
			t.Errorf("Error in request: %#v", resp.Error)
		}
	}
}

func TestDeleteUser(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := http.NewRequest("DELETE", "/api/users/03a1453a-949e-4613-9032-862d0567dfd1", nil)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Error("Error in request")
	}
}
