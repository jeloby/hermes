package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m *Manager) ticketWatcherRouter(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.With(m.Paginate, m.Filter).Get("/", m.getAllTicketWatchers)
		r.Post("/", m.createTicketWatcher)
		r.Put("/", m.updateTicketWatcher)
		r.Delete("/{ticketID}", m.deleteTicketWatcher)
	})
}

func (m *Manager) getAllTicketWatchers(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) createTicketWatcher(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) updateTicketWatcher(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) deleteTicketWatcher(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}
