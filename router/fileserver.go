package router

import (
	"fmt"
	"net/http"
	"path/filepath"
	"time"

	"bitbucket.org/jeloby/go_utils/sessions"
	"github.com/go-chi/chi/v5"
)

// A completely separate router for administrator routes
func (m *Manager) fileServer() http.Handler {
	r := chi.NewRouter()
	// Assets Router
	r.Get("/js/{file}", m.getJavascript)
	r.Get("/css/{file}", m.getStyleSheets)
	r.Get("/img/{file}", m.getImages)
	r.Get("/favicon.ico", m.getFavIcon)

	// Open Routes
	r.Get("/login", m.loginapp)

	r.Get("/set", m.setSession)

	// Auth Routes
	r.Get("/*", m.mainapp)

	return r
}

func (m *Manager) getJavascript(w http.ResponseWriter, r *http.Request) {
	file := chi.URLParam(r, "file")
	http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/js/", file))
}

func (m *Manager) getStyleSheets(w http.ResponseWriter, r *http.Request) {
	file := chi.URLParam(r, "file")
	http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/css/", file))
}

func (m *Manager) getImages(w http.ResponseWriter, r *http.Request) {
	file := chi.URLParam(r, "file")
	http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/img/", file))
}

func (m *Manager) getFavIcon(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/favicon.ico"))
}

// func (m *Manager) loadLoginApp(w http.ResponseWriter, r *http.Request) {
// 	var (
// 		loggedIn bool
// 		cookie   string
// 	)
// 	defer func() {
// 		if loggedIn {
// 			http.Redirect(w, r, "/", http.StatusAccepted)
// 			m.mainapp(w, r)
// 			return
// 		}
// 		m.loginapp(w, r)
// 	}()
//
// 	cookie = m.checkCookie(r)
//
// 	_, err := m.SessionStore.Get(cookie)
// 	if err != nil {
// 		fmt.Printf("ERROR: %s", err)
// 		return
// 	}
//
// 	loggedIn = true
// }

// func (m *Manager) loadApplication(w http.ResponseWriter, r *http.Request) {
// 	var (
// 		// loggedIn bool
// 		cookie string
// 	)
//
// 	cookie = m.checkCookie(r)
//
// 	_, err := m.SessionStore.Get(cookie)
// 	if err != nil {
// 		fmt.Printf("ERROR: %s", err)
// 		return
// 	}
//
// 	// loggedIn = true
// }

func (m *Manager) loginapp(w http.ResponseWriter, r *http.Request) {
	var (
		err     error
		logedIn bool
	)

	defer func() {
		if logedIn {
			http.Redirect(w, r, "/", http.StatusOK)
		}

		http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/loginapp.html"))
	}()

	c, err := r.Cookie("session_token")
	if err != nil {
		m.log.Errorf("failed to check cookie %#v", err)
		return
	}

	chacked_session, err := m.sessionStore.Get(c.Value)
	if err != nil {
		m.log.Errorf("failed to check session %#v", err)
		return
	}

	if chacked_session.Valid() {
		logedIn = true
	}
}

func (m *Manager) mainapp(w http.ResponseWriter, r *http.Request) {
	fmt.Println("MainApp")
	http.ServeFile(w, r, filepath.Join(m.templateDirectory, "/mainapp.html"))
}

func (m *Manager) setSession(w http.ResponseWriter, r *http.Request) {
	data := sessions.CachedSession{
		UserID: "someuser",
	}

	sessionkey, err := m.sessionStore.Set(&data)
	if err != nil {
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   sessionkey,
		Expires: time.Now().Add(600 * time.Minute),
	})
}

// func (m *Manager) checkCookie(r *http.Request) string {
// 	c, err := r.Cookie("session_token")
// 	if err != nil {
// 		if err == http.ErrNoCookie {
// 			return ""
// 		} else {
// 			errors.Log(err)
// 			return ""
// 		}
// 	}
//
// 	return c.Value
// }
