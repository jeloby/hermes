package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/jcrypt"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/hermes/models"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m Manager) userRouter(r chi.Router) {
	r.With(m.Paginate, m.Filter).Get("/", m.GetAllUsers)
	r.Get("/{userID}", m.getUser)
	r.Post("/", m.createUser)
	r.Put("/{userID}", m.updateUser)
	r.Delete("/{userID}", m.deleteUser)
}

func (m Manager) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		filter     *common.Filter
		pagination *common.Pagination
		users      []*models.User
		ok         bool
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: users, Pagination: pagination})
	}()

	filter, ok = r.Context().Value(filterContext).(*common.Filter)
	if !ok {
		safe, err = errors.Wrapper(nil, filterError)
		return
	}

	pagination, ok = r.Context().Value(paginationContext).(*common.Pagination)
	if !ok {
		safe, err = errors.Wrapper(nil, paginationError)
		return
	}

	users, pagination, err = m.UserService.SelectAll(filter, pagination)
}

func (m Manager) getUser(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		safe   string
		userID string
		user   *models.User
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: user})
	}()

	userID = chi.URLParam(r, "userID")

	user, err = m.UserService.Select(&common.Filter{Raw: "id='" + userID + "'"})
}

func (m Manager) createUser(w http.ResponseWriter, r *http.Request) {
	var (
		safe string
		err  error
		user models.User
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	user = models.User{}

	err = render.Bind(r, &user)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	err = user.InitDefaults()
	if err != nil {
		safe, err = errors.Wrapper(err, setDafaultError)
		return
	}

	user.Password = jcrypt.GeneratePassword(user.Password)

	err = user.Validate()
	if err != nil {
		safe, err = errors.Wrapper(err, validationError)
		return
	}

	err = m.UserService.Create(&user)
}

func (m Manager) updateUser(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		safe   string
		user   models.User
		userID string
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	userID = chi.URLParam(r, "userID")
	user = models.User{}

	err = render.Bind(r, &user)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	user.ID = userID

	err = m.UserService.Update(&user)
}

func (m Manager) deleteUser(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		safe   string
		userID string
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	userID = chi.URLParam(r, "userID")

	err = m.UserService.Delete(&common.Filter{Raw: "id='" + userID + "'"})
}
