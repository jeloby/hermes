package router

import (
	"time"

	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/sessions"
	"bitbucket.org/jeloby/hermes/config"
	"bitbucket.org/jeloby/hermes/models"
	"github.com/go-chi/jwtauth"
)

const (
	apiCallError    = "failed api call route: %s, %#v"
	filterError     = "cannot assert filter"
	paginationError = "cannot assert pagination"
	paramsBindError = "failed to bind params to model"
	validationError = "failed to validate the model"
	setDafaultError = "failed to init model defaults"
)

var store = sessions.NewCookieStore("123456789qwerty")

// BasicResponse is a general API response type
type BasicResponse struct {
	Status     string             `json:"status" validate:"required"`
	Error      string             `json:"error,omitempty"`
	Data       interface{}        `json:"data,omitempty"`
	Pagination *common.Pagination `json:"pagination,omitempty"`
}

// Manager acts as an adapter between the domain and the HTTP protocol.
type Manager struct {
	tokenAuth          *jwtauth.JWTAuth
	tokenTimeout       time.Duration
	HTTPRequestTimeout time.Duration
	templateDirectory  string
	log                *logger.Log
	sessionStore       *sessions.SessionManager

	// Service Mapping
	UserService models.UserService
	OrgService  models.OrgService

	// Ticket Services
	TicketService        models.TicketService
	TicketCommentService models.TicketCommentService
	TicketWatcherService models.TicketWatcherService
}

// NewManager returns a new Manager router wrapper.
func NewManager(conf config.Server, log *logger.Log, sessionStore *sessions.SessionManager) *Manager {
	return &Manager{
		tokenAuth:          jwtauth.New("HS512", []byte(conf.Secret), nil),
		tokenTimeout:       conf.TokenTimeout.Duration,
		HTTPRequestTimeout: conf.HTTPRequestTimeout.Duration,
		templateDirectory:  conf.TempDir,
		log:                log,
		sessionStore:       sessionStore,
	}
}
