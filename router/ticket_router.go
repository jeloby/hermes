package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/hermes/models"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m *Manager) ticketRouter(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.With(m.Paginate, m.Filter).Get("/", m.getAllTickets)
		r.Get("/{ticketID}", m.getTicket)
		r.Post("/", m.createTicket)
		r.Put("/{ticketID}", m.updateTicket)
		r.Delete("/{ticketID}", m.deleteTicket)
	})
}

func (m *Manager) getAllTickets(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		filter     *common.Filter
		pagination *common.Pagination
		tickets    []*models.Ticket
		ok         bool
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: tickets, Pagination: pagination})
	}()

	filter, ok = r.Context().Value(filterContext).(*common.Filter)
	if !ok {
		safe, err = errors.Wrapper(nil, filterError)
		return
	}

	pagination, ok = r.Context().Value(paginationContext).(*common.Pagination)
	if !ok {
		safe, err = errors.Wrapper(nil, paginationError)
		return
	}

	tickets, pagination, err = m.TicketService.SelectAll(filter, pagination)
}

func (m *Manager) getTicket(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		safe     string
		ticket   *models.Ticket
		ticketID string
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: ticket})
	}()

	ticketID = chi.URLParam(r, "ticketID")

	ticket, err = m.TicketService.Select(&common.Filter{Raw: "id='" + ticketID + "'"})
}

func (m *Manager) createTicket(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		safe   string
		ticket models.Ticket
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: ""})
	}()

	err = render.Bind(r, &ticket)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	err = m.TicketService.Create(&ticket)
}

func (m *Manager) updateTicket(w http.ResponseWriter, r *http.Request) {
	var (
		err    error
		safe   string
		ticket models.Ticket
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: ""})
	}()

	err = render.Bind(r, &ticket)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	ticket.ID = chi.URLParam(r, "ticketID")

	err = m.TicketService.Update(&ticket)
}

func (m *Manager) deleteTicket(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		safe     string
		ticketID string
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: ""})
	}()

	ticketID = chi.URLParam(r, "ticketID")

	err = m.TicketService.Delete(&common.Filter{Raw: "id='" + ticketID + "'"})
}
