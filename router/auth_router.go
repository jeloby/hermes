package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/jcrypt"
	"bitbucket.org/jeloby/hermes/models"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m *Manager) authRouter(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.Post("/register", m.registerUser)
		r.Post("/login", m.login)
	})
}

func (m *Manager) registerUser(w http.ResponseWriter, r *http.Request) {
	var (
		safe    string
		err     error
		account models.Register
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	err = render.Bind(r, &account)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	err = account.User.InitDefaults()
	if err != nil {
		safe, err = errors.Wrapper(err, setDafaultError)
		return
	}

	err = account.Org.InitDefaults()
	if err != nil {
		safe, err = errors.Wrapper(err, setDafaultError)
		return
	}

	account.User.Password = jcrypt.GeneratePassword(account.User.Password)

	err = account.User.Validate()
	if err != nil {
		safe, err = errors.Wrapper(err, validationError)
		return
	}

	err = account.Org.Validate()
	if err != nil {
		safe, err = errors.Wrapper(err, validationError)
		return
	}

	err = m.OrgService.Create(&models.Org{Org: account.Org})
	if err != nil {
		return
	}

	account.User.OrgID = account.Org.GetID()

	err = m.UserService.Create(&models.User{User: account.User})
}

func (m *Manager) login(w http.ResponseWriter, r *http.Request) {
}
