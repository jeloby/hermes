package router

import (
	"encoding/json"
	"net/http"
	"testing"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/hermes/models"
)

var mockGetOrg = user.User{ // User test response
	ID:                "03a1453a-949e-4613-9032-862d0567dfd1",
	OrgID:             "03a1453a-949e-4613-9032-862d0567dfd2",
	FirstName:         "Stefan",
	LastName:          "Moller",
	Email:             "stefanmoller@live.com",
	Phone:             "0828955572",
	Role:              common.Role_Admin,
	PasswordUpdatedAt: 1641205072,
	Payload:           "",
	NotesID:           "03a1453a-949e-4613-9032-862d0567dfd3",
	CreatedBy:         "03a1453a-949e-4613-9032-862d0567dfd4",
	CreatedAt:         1641205072,
	UpdatedAt:         1641215072,
	DeletedAt:         0,
}

var mockCreateOrg = user.User{ // User create request
	OrgID:     "03a1453a-949e-4613-9032-862d0567dfd2",
	FirstName: "Stefan",
	LastName:  "Moller",
	Email:     "stefanmoller@live.com",
	Phone:     "0828955572",
	Password:  "helloworld",
	Role:      common.Role_Admin,
	Payload:   "",
	CreatedBy: "03a1453a-949e-4613-9032-862d0567dfd4",
}

var mockUpdateOrg = user.User{ // User create request
	ID:        "03a1453a-949e-4613-9032-862d0567dfd1",
	OrgID:     "03a1453a-949e-4613-9032-862d0567dfd2",
	FirstName: "Stefan",
	LastName:  "Moller",
	Email:     "stefanmoller@live.com",
	Phone:     "0828955572",
	Password:  "helloworld",
	Role:      common.Role_Admin,
	Payload:   "",
	NotesID:   "03a1453a-949e-4613-9032-862d0567dfd3",
	CreatedBy: "03a1453a-949e-4613-9032-862d0567dfd4",
}

type OrgService struct{}

func (u OrgService) SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*models.User, *common.Pagination, error) {
	if err := checkFilter(filter); err != nil {
		return nil, nil, err
	}

	if err := checkPagination(pagination); err != nil {
		return nil, nil, err
	}

	return []*models.User{{User: &mockGetOrg}}, nil, nil
}

func (u OrgService) Select(filter *common.Filter) (*models.User, error) {
	if err := checkFilter(filter); err != nil {
		return nil, err
	}

	return &models.User{User: &mockGetOrg}, nil
}

func (u OrgService) Create(model *models.User) error {
	return model.Validate()
}

func (u OrgService) Update(model *models.User) error {
	return model.Validate()
}

func (u OrgService) Delete(filter *common.Filter) error {
	if err := checkFilter(filter); err != nil {
		return err
	}

	return nil
}

func TestSelectAllOrgs(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := http.NewRequest("GET", "/api/users?q=company_id=12345", nil)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Error("Error in request")
	}
}

func TestSelectOrg(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := http.NewRequest("GET", "/api/users/03a1453a-949e-4613-9032-862d0567dfd1", nil)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Error("Error in request")
	}
}

func TestCreateOrg(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	body := requestBody(&mockCreateOrg)

	req, err := caller.request("POST", "/api/users", body)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Errorf("Error in request: %#v", resp.Error)
	}
}

func TestUpdateOrg(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := caller.request("PUT", "/api/users/03a1453a-949e-4613-9032-862d0567dfd1", requestBody(&mockUpdateOrg))
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Errorf("Error in request: %#v", resp.Error)
	}
}

func TestDeleteOrg(t *testing.T) {
	caller := createRouterTest()
	caller.useFilter()
	caller.usePagination()

	req, err := http.NewRequest("DELETE", "/api/users/03a1453a-949e-4613-9032-862d0567dfd1", nil)
	if err != nil {
		t.Errorf("Error creating a new request: %v", err)
	}

	caller.serve(req)

	if status := caller.recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code. Expected: %d. Got: %d.", http.StatusOK, status)
	}

	var resp BasicResponse
	if err := json.NewDecoder(caller.recorder.Body).Decode(&resp); err != nil {
		t.Errorf("Error decoding response body: %v", err)
	}

	if resp.Status != "ok" {
		t.Error("Error in request")
	}
}
