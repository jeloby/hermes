package router

// Response Statuses
const (
	status_ok    = "ok"
	status_error = "error"
)

// Error Messages
// This should be a standardise list of error responses to
// ensure consistance accross an app.
const (
	// invalidParams = "invalid params"
	// permissionDenied = "permission denied"
	sessionLoadFailed = "failed to load session"
)

const (
	// Session Identifiers
	SessionName = "session"
	SessionKey  = "key"
)
