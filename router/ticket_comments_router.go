package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m *Manager) ticketCommentRouter(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.With(m.Paginate, m.Filter).Get("/", m.getAllTicketComments)
		r.Get("/{ticketCommentID}", m.getTicketComment)
		r.Post("/", m.createTicketComment)
		r.Put("/", m.updateTicketComment)
		r.Delete("/{ticketCommentID}", m.deleteTicketComment)
	})
}

func (m *Manager) getAllTicketComments(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) getTicketComment(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) createTicketComment(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) updateTicketComment(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}

func (m *Manager) deleteTicketComment(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		pagination *common.Pagination
		// orgs       []*app.
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: "", Pagination: pagination})
	}()
}
