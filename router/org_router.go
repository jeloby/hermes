package router

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/hermes/models"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

func (m *Manager) orgsRouter(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.With(m.Paginate, m.Filter).Get("/", m.getAllOrgs)
		r.Get("/{orgID}", m.getOrg)
		r.Post("/", m.createOrg)
		r.Put("/", m.updateOrg)
		r.Delete("/{orgID}", m.deleteOrg)
	})
}

func (m *Manager) getAllOrgs(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		safe       string
		filter     *common.Filter
		pagination *common.Pagination
		orgs       []*models.Org
		ok         bool
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: orgs, Pagination: pagination})
	}()

	filter, ok = r.Context().Value(filterContext).(*common.Filter)
	if !ok {
		safe, err = errors.Wrapper(nil, filterError)
		return
	}

	pagination, ok = r.Context().Value(paginationContext).(*common.Pagination)
	if !ok {
		safe, err = errors.Wrapper(nil, paginationError)
		return
	}

	orgs, pagination, err = m.OrgService.SelectAll(filter, pagination)
}

func (m *Manager) getOrg(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		safe  string
		orgID string
		org   *models.Org
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok, Data: org})
	}()

	orgID = chi.URLParam(r, "orgID")
	if orgID == "" {
		err = errors.New("missing org id")
		return
	}

	org, err = m.OrgService.Select(&common.Filter{Raw: "id='" + orgID + "'"})
}

func (m *Manager) createOrg(w http.ResponseWriter, r *http.Request) {
	var (
		safe string
		err  error
		org  models.Org
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	org = models.Org{}

	err = render.Bind(r, &org)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	err = org.InitDefaults()
	if err != nil {
		safe, err = errors.Wrapper(err, setDafaultError)
		return
	}

	err = org.Validate()
	if err != nil {
		safe, err = errors.Wrapper(err, validationError)
		return
	}

	m.log.Debugf("%#v", org.Org)

	err = m.OrgService.Create(&org)
}

func (m *Manager) updateOrg(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		safe  string
		orgID string
		org   models.Org
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	org = models.Org{}

	err = render.Bind(r, &org)
	if err != nil {
		safe, err = errors.Wrapper(err, paramsBindError)
		return
	}

	orgID = chi.URLParam(r, "orgID")
	if orgID == "" {
		err = errors.New("missing org id")
		return
	}

	org.ID = orgID

	err = org.InitDefaults()
	if err != nil {
		safe, err = errors.Wrapper(err, setDafaultError)
		return
	}

	err = m.OrgService.Update(&org)
}

func (m *Manager) deleteOrg(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		safe  string
		orgID string
	)

	defer func() {
		if err != nil {
			m.log.Errorf(apiCallError, r.URL.String(), err)
			render.JSON(w, r, &BasicResponse{Status: status_error, Error: safe})

			return
		}

		render.JSON(w, r, &BasicResponse{Status: status_ok})
	}()

	orgID = chi.URLParam(r, "orgID")
	if orgID == "" {
		err = errors.New("missing org id")
		return
	}

	err = m.OrgService.Delete(&common.Filter{Raw: "id='" + orgID + "'"})
}
