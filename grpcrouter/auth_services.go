package grpcrouter

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/jeloby/go_utils/jcrypt"
	"bitbucket.org/jeloby/go_utils/models/common"
	usermodel "bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/hermes/models"
)

type AuthServices struct {
	usermodel.UnimplementedUserServiceServer
	UserService models.UserService
}

func (s *AuthServices) Login(ctx context.Context, in *usermodel.UserRequest) (*usermodel.UserResponse, error) {
	var (
		users      []*models.User
		pagination *common.Pagination
		err        error
		tokens     *SessionTokens
	)

	tokens, err = getSessionToken(ctx)
	if err != nil {
		return nil, errors.New("invalid session token")
	}
	fmt.Printf("token: %s,key: %s", tokens.token, tokens.key)

	users, pagination, err = s.UserService.SelectAll(in.GetFilter(), in.GetPagination())
	if err != nil {
		return nil, err
	}

	userResponse := make([]*usermodel.User, len(users))
	for i, u := range users {
		userResponse[i] = u.User
	}

	return &usermodel.UserResponse{
		Users:      userResponse,
		Pagination: pagination,
	}, nil
}

func (s *AuthServices) Register(ctx context.Context, in *usermodel.UserRequest) (*usermodel.UserResponse, error) {
	var (
		user *models.User
		err  error
	)

	user, err = s.UserService.Select(in.GetFilter())
	if err != nil {
		return nil, err
	}

	userResponse := make([]*usermodel.User, 1)
	userResponse[0] = user.User

	return &usermodel.UserResponse{
		Users: userResponse,
	}, nil
}

func (s *UserServices) ResetPassword(ctx context.Context, in *usermodel.UserRequest) (*usermodel.UserResponse, error) {
	var (
		user models.User
		err  error
	)

	user = models.User{User: in.GetUser()}

	err = user.InitDefaults()
	if err != nil {
		return nil, err
	}

	user.Password = jcrypt.GeneratePassword(user.Password)

	err = user.Validate()
	if err != nil {
		// safe, err = errors.Wrapper(err, validationError)
		return nil, err
	}

	err = s.UserService.Create(&user)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
