package grpcrouter

import (
	"context"
	"net"

	"bitbucket.org/jeloby/go_utils/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type grpcServer struct {
	host   string
	port   string
	caCert string
	cert   string
	key    string

	Server   *grpc.Server
	listener net.Listener
}

func NewServer(host, port, caCert, cert, key string) *grpcServer {
	return &grpcServer{
		host:   host,
		port:   port,
		caCert: caCert,
		cert:   cert,
		key:    key,
	}
}

func (g *grpcServer) ConnectServer() (err error) {
	var opts []grpc.ServerOption

	g.listener, err = net.Listen("tcp", ":"+g.port)
	if err != nil {
		return errors.Wrap(err, "failed to listen")
	}

	if g.cert != "" && g.key != "" {
		creds, err := credentials.NewServerTLSFromFile(g.cert, g.key)
		if err != nil {
			return errors.Wrap(err, "Failed to generate credentials")
		}

		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}

	g.Server = grpc.NewServer(opts...)

	return nil
}

func (g *grpcServer) Serve() error {
	return g.Server.Serve(g.listener)
}

type SessionTokens struct {
	token string
	key   string
}

func getSessionToken(ctx context.Context) (*SessionTokens, error) {
	var (
		headers    metadata.MD
		ok         bool
		token, key string
	)

	headers, ok = metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, errors.New("failed to load session token")
	}

	_token := headers.Get("jtoken")
	if len(_token) > 0 {
		token = _token[0]
	}

	_key := headers.Get("jkey")
	if len(_key) > 0 {
		key = _key[0]
	}

	return &SessionTokens{
		token: token,
		key:   key,
	}, nil
}
