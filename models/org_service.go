package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/org"
)

type Org struct {
	*org.Org
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (p *Org) Bind(r *http.Request) error {
	return nil
}

type OrgService interface {
	SelectAll(*common.Filter, *common.Pagination) ([]*Org, *common.Pagination, error)
	Select(*common.Filter) (*Org, error)
	Create(*Org) error
	Update(*Org) error
	Delete(*common.Filter) error
}
