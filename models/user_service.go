package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/user"
)

type User struct {
	*user.User
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (p *User) Bind(r *http.Request) error {
	return nil
}

type UserService interface {
	SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*User, *common.Pagination, error)
	Select(*common.Filter) (*User, error)
	Create(*User) error
	Update(*User) error
	Delete(*common.Filter) error
}
