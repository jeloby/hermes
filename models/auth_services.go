package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/org"
	"bitbucket.org/jeloby/go_utils/models/user"
)

type Register struct {
	*org.Org
	*user.User
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (p *Register) Bind(r *http.Request) error {
	return nil
}
