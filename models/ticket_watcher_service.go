package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/tickets"
)

type TicketWatcher struct {
	*tickets.TicketWatcher
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (tw *TicketWatcher) Bind(r *http.Request) error {
	return nil
}

type TicketWatcherService interface {
	SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*TicketWatcher, *common.Pagination, error)
	Create(*TicketWatcher) error
	Update(*TicketWatcher) error
	Delete(*common.Filter) error
}
