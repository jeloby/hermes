package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/tickets"
)

type Ticket struct {
	*tickets.Ticket
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (p *Ticket) Bind(r *http.Request) error {
	return nil
}

type TicketService interface {
	SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*Ticket, *common.Pagination, error)
	Select(*common.Filter) (*Ticket, error)
	Create(*Ticket) error
	Update(*Ticket) error
	Delete(*common.Filter) error
}
