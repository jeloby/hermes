package models

import (
	"net/http"

	"bitbucket.org/jeloby/go_utils/models/common"
	"bitbucket.org/jeloby/go_utils/models/tickets"
)

type TicketComment struct {
	*tickets.TicketComment
}

// Bind implements the chi Binder interface that runs after it has been unmarshalled.
func (tc *TicketComment) Bind(r *http.Request) error {
	return nil
}

type TicketCommentService interface {
	SelectAll(filter *common.Filter, pagination *common.Pagination) ([]*TicketComment, *common.Pagination, error)
	Select(*common.Filter) (*TicketComment, error)
	Create(*TicketComment) error
	Update(*TicketComment) error
	Delete(*common.Filter) error
}
