package main

import (
	"flag"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/jeloby/go_utils/db"
	"bitbucket.org/jeloby/go_utils/errors"
	"bitbucket.org/jeloby/go_utils/logger"
	"bitbucket.org/jeloby/go_utils/models/user"
	"bitbucket.org/jeloby/go_utils/nats_worker"
	"bitbucket.org/jeloby/go_utils/sessions"
	"bitbucket.org/jeloby/hermes/config"
	"bitbucket.org/jeloby/hermes/grpcrouter"
	"bitbucket.org/jeloby/hermes/messenger"
	"bitbucket.org/jeloby/hermes/router"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc/reflection"
)

const (
	name    = "hermes"
	version = "0.1.0"
)

func main() {
	var (
		err        error
		conf       *config.Config
		natsConn   *nats_worker.Connection
		configFile *string
		debug      *bool
	)

	// Load CLI configuration
	configFile = flag.String("c", "/etc/"+name+"/config.toml", "path to the config file")
	debug = flag.Bool("debug", false, "add additional debug information to the log output")

	// Parse CLI flags.
	flag.Parse()

	// Init new loger instance
	var log = logger.New(*debug, name)

	log.Infof("Initialized new logger on: %s, debug: %t\n", name, *debug)
	log.Infof("Initialized %s service version: %s\n", name, version)

	// Load Configuration
	conf, err = config.NewConfig(*configFile, name)
	errors.Panicf(err, "failed load config")

	// Create redis instance
	cache := db.NewCacheManager()

	err = cache.NewRedisClient(conf.Redis.Address, conf.Redis.Password, conf.Redis.DB)
	errors.Panic(err)

	defer cache.Close()

	natsManager := nats_worker.New(
		conf.Nats.Servers,
		conf.Nats.User,
		conf.Nats.Password,
		conf.Nats.TLS.CACert,
		conf.Nats.TLS.Cert,
		conf.Nats.TLS.Key,
		conf.Nats.RequestTimeout.Duration,
	)

	natsConn, err = natsManager.Connect()
	errors.Panicf(err, "unable to connect to nats server")

	server := router.NewManager(conf.Server, log, sessions.NewSessionManager(name, cache.Redis))

	// Register Services
	userService := &messenger.UserService{DB: natsConn, Log: log, Timeout: 60 * time.Second}

	// User Rest Router
	server.UserService = userService
	server.OrgService = &messenger.OrgService{DB: natsConn, Log: log, Timeout: 60 * time.Second}

	// Ticket Rest Router
	server.TicketService = &messenger.TicketService{DB: natsConn, Log: log, Timeout: 60 * time.Second}
	server.TicketCommentService = &messenger.TicketCommentService{DB: natsConn, Log: log, Timeout: 60 * time.Second}
	server.TicketWatcherService = &messenger.TicketWatcherService{DB: natsConn, Log: log, Timeout: 60 * time.Second}

	listenAddr := conf.Server.Server + ":" + conf.Server.Port

	http.Handle("/metrics", promhttp.Handler())

	gserver := grpcrouter.NewServer(
		conf.GRPC.Server,
		conf.GRPC.Port,
		conf.GRPC.TLS.CACert,
		conf.GRPC.TLS.Cert,
		conf.GRPC.TLS.Key,
	)

	err = gserver.ConnectServer()
	if err != nil {
		log.Fatalf("unable to connect to grpc server %+v", err)
	}

	user.RegisterUserServiceServer(gserver.Server, grpcrouter.UserServices{
		UserService: userService,
	})
	reflection.Register(gserver.Server)

	go func() {
		err = gserver.Serve()
		if err != nil {
			log.Fatalf("unable to start listening to grpc server %+v", err)
		}
	}()

	for _, server := range gserver.Server.GetServiceInfo() {
		for _, endpoint := range server.Methods {
			fmt.Printf("Starting service %v on model %v\n", endpoint.Name, server.Metadata)

		}
	}

	go func() {
		err := http.ListenAndServe(":8030", nil)
		if err != nil {
			log.Fatalf("unable to start metrics router %#v", err)
		}
	}()

	if conf.Server.Enabled {
		log.Infof("starting hermes v1 API server %s", conf.Server.Port)

		err = http.ListenAndServeTLS(listenAddr, conf.Server.TLS.Cert, conf.Server.TLS.Key, server.Router())
		if err != nil {
			log.Fatalf("error starting server, %+v", err)
		}
	}
}
