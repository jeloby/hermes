# GENERATE CERTS

## Basic TSL certificates

#### Genrate CA Key
```bash
$ openssl genrsa -aes256 -out ./etc/tls/private/ca.key.pem 4096
```

#### Generate CA Cert
```bash
$ openssl req -config ./etc/tls/ca_config.conf -key ./etc/tls/private/ca.key.pem -new -x509 -days 3650 -sha256 -extensions v3_ca -out ./etc/tls/certs/ca.crt.pem
```

#### GENERATE Private Key

```bash
$ openssl genrsa -out ./etc/tls/private/private.key.pem 4096                                             
```

#### Generate CSR

```bash
$ openssl req -new -key ./etc/tls/private/private.key.pem -out ./etc/tls/service.csr -config ./etc/tls/cert_config.conf 
```

#### Generate Cert

```bash
$ openssl x509 -req -in ./etc/tls/service.csr -CA ./etc/tls/certs/ca.crt.pem -CAkey ./etc/tls/private/ca.key.pem -CAcreateserial -out ./etc/tls/certs/client.crt.pem -days 825 -sha256  -extensions req_ext -extfile ./etc/tls/cert_config.conf
```